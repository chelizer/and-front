import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Activity } from './activity.model';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  apiUrl = 'http://100.24.232.62:8085/api/DEPARTAMENTO'

  constructor(private http: HttpClient) { }

  getActivities(): Observable<Activity[]> {
    return this.http.get<Activity[]>(`${this.apiUrl}`).pipe(
      tap((_) => console.info('fetched activities', _)),
      catchError(this.handleError<Activity[]>('getActivities', []))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
