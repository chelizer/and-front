import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Activity } from '../activity.model';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css'],
})
export class ActivityComponent implements OnInit {
  activities: Activity[] = [];
  // activities$: Observable<Activity[]>

  constructor(private backendService: BackendService) {}

  ngOnInit(): void {
    this.getResults();
  }

  getResults(): void {
    this.backendService
      .getActivities()
      .subscribe((activities) => (this.activities = activities));
  }
}
