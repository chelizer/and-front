export interface Activity {
  depCodigo: string;
  depNombre: string;
  links?: string;
}
